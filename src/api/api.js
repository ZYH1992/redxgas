import axios from 'axios';

//let base = 'http://47.93.199.82:8090/sellserver';

let base = 'http://127.0.0.1:8090/sellserver';
axios.defaults.headers['Content-Type']='application/x-www-form-urlencoded';
//axios.defaults.headers.common['Content-Type'] = 'application/json;charset=UTF-8';
export const requestLogin = params => { return axios.post(`${base}/user/login`, params,{emulateJSON: true}).then(res => res); };

export const getUserList = params => { return axios.post(`${base}/user/getuserlist`, { params: params },{emulateJSON: true}).then(res => res); };

export const getUserID = params => { return axios.get(`${base}/user/getUserID`, { params: params },{emulateJSON: true}).then(res => res); };

export const getUserInfo = params => { return axios.post(`${base}/user/getUserInfo`, { params: params },{emulateJSON: true}).then(res => res); };

export const getUserListPage = params => { return axios.get(`${base}/user/listpage`, { params: params }).then(res => res); };

export const removeUser = params => { return axios.get(`${base}/user/remove`, { params: params }).then(res => res); };

export const batchRemoveUser = params => { return axios.get(`${base}/user/batchremove`, { params: params }).then(res => res); };

export const editUser = params => { return axios.get(`${base}/user/edit`, { params: params }).then(res => res); };

export const addUser = params => { return axios.post(`${base}/user/adduser`, { params: params }).then(res => res); };
/**充值链接 */
export const doCharge = params => { return axios.post(`${base}/charge/doCharge`, { params: params },{emulateJSON: true}).then(res => res); };

export const getChargeList = params => { return axios.post(`${base}/charge/getChargeList`, { params: params },{emulateJSON: true}).then(res => res); };
/**阶梯气价连接 */
export const setStairPrice = params => { return axios.post(`${base}/stairprice/setStairPrice`, { params: params },{emulateJSON: true}).then(res => res); };
export const getAllStairPrice = params => { return axios.post(`${base}/stairprice/getAllStairPrice`, { params: params },{emulateJSON: true}).then(res => res); };
export const searchStairPrice = params => { return axios.post(`${base}/stairprice/searchStairPrice`, { params: params },{emulateJSON: true}).then(res => res); };
export const updateStairPrice = params => { return axios.post(`${base}/stairprice/updateStairPrice`, { params: params },{emulateJSON: true}).then(res => res); };
export const removeStairPrice = params => { return axios.post(`${base}/stairprice/removeStairPrice`, { params: params },{emulateJSON: true}).then(res => res); };
export const getStairPriceName = params => { return axios.post(`${base}/stairprice/getStairPriceName`, { params: params },{emulateJSON: true}).then(res => res); };
/**生效日期 */
export const setEffectDate = params => { return axios.post(`${base}/stairprice/setEffectDate`, { params: params },{emulateJSON: true}).then(res => res); };
export const getAllEffectDate = params => { return axios.post(`${base}/stairprice/getAllEffectDate`, { params: params },{emulateJSON: true}).then(res => res); };
export const updateEffectDate = params => { return axios.post(`${base}/stairprice/updateEffectDate`, { params: params },{emulateJSON: true}).then(res => res); };
export const removeEffectDate = params => { return axios.post(`${base}/stairprice/removeEffectDate`, { params: params },{emulateJSON: true}).then(res => res); };
//删除时 加判断如果有用户在使用 则无法删除
export const getEffectDateName = params => { return axios.post(`${base}/stairprice/getEffectDateName`, { params: params },{emulateJSON: true}).then(res => res); };
export const searchEffectdateName = params => { return axios.post(`${base}/stairprice/searchEffectdateName`, { params: params },{emulateJSON: true}).then(res => res); };