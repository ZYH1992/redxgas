import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Main from './views/Main.vue'
import Table from './views/nav1/Table.vue'
import Form from './views/nav1/Form.vue'
import user from './views/nav1/user.vue'
import Page4 from './views/nav2/Page4.vue'
import Page5 from './views/nav2/Page5.vue'
import Page6 from './views/nav3/Page6.vue'
import echarts from './views/charts/echarts.vue'
/**用户管理页面 */
import CreateUser from './views/usermanager/CreateUser.vue'
import SearchUser from './views/usermanager/SearchUser.vue'
import EditUser from './views/usermanager/EditUser.vue'
/**收费管理页面 */
import ChargeGas from './views/sellmanager/ChargeGas.vue'
import SearchRecord from './views/sellmanager/SearchRecord.vue'
/**表具管理 */
import TabControl from './views/metermanager/TabControl.vue'
import ReadHistory from './views/metermanager/ReadHistory.vue'

import ParamSet from './views/metermanager/ParamSet.vue'
import ReadRecord from './views/metermanager/ReadRecord.vue'
/**价格管理 */
import CreatePrice from './views/pricemanager/CreatePrice.vue'
import EditPrice from './views/pricemanager/EditPrice.vue'
import CreateDate from './views/pricemanager/CreateDate.vue'
import EditDate from './views/pricemanager/EditDate.vue'
import CommonParamSet from './views/pricemanager/CommonParamSet.vue'
let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    //{ path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '用户管理',
        disabled : 'true',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/createuser', component: CreateUser, name: '用户建档'},
            { path: '/searchuser', component: SearchUser, name: '用户查询' },
            { path: '/edituser', component: EditUser, name: '档案变更' },
        ]
    },
    {
        path: '/',
        component: Home,
        name: '收费管理',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/chargegas', component: ChargeGas, name: '收费'},
            { path: '/searchrecord', component: SearchRecord, name: '收费查询' }
           
        ]
    },
    {
        path: '/',
        component: Home,
        name: '表具管理',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/tabcontrol', component: TabControl, name: '阀门控制'},
            { path: '/paramset', component: ParamSet, name: '表参数设置' },
            { path: '/readhistory', component: ReadHistory, name: '历史数据读取' },
            { path: '/readrecord', component: ReadRecord, name: '抄表' },
           
        ]
    },
    {
        path: '/',
        component: Home,
        name: '价格管理',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/createprice', component: CreatePrice, name: '阶梯价格设置'},
            { path: '/editprice', component: EditPrice, name: '阶梯价格修改' },
            { path: '/createdate', component: CreateDate, name: '生效日期设置' },
            { path: '/editdate', component: EditDate, name: '生效日期修改' },
            { path: '/commonparamset', component: CommonParamSet, name: '公共参数设置' }
           
        ]
    },
    {
        path: '/',
        component: Home,
        name: '综合查询',
        iconCls: 'el-icon-message',//图标样式class
       
    },
    {
        path: '/',
        component: Home,
        name: '导航一',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/main', component: Main, name: '', hidden: true },
            { path: '/table', component: Table, name: 'Table' },
            { path: '/form', component: Form, name: 'Form' },
            { path: '/user', component: user, name: '列表' },
        ]
    },
    /**
    {
        path: '/',
        component: Home,
        name: '导航二',
        iconCls: 'fa fa-id-card-o',
        children: [
            { path: '/page4', component: Page4, name: '页面4' },
            { path: '/page5', component: Page5, name: '页面5' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-address-card',
        leaf: true,//只有一个节点
        children: [
            { path: '/page6', component: Page6, name: '导航三' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: 'Charts',
        iconCls: 'fa fa-bar-chart',
        children: [
            { path: '/echarts', component: echarts, name: 'echarts' }
        ]
    },*/
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;